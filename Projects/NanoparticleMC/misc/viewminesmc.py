"""generates atoms object from .smc file and views in ase gui
usage: python viewCluster.py smcfile
where smcfile is the full path to .smc file
"""
from __future__ import print_function
import sys
from montecarlo import *
from ase.cluster.cubic import FaceCenteredCubic
import ase
from data import fcc
from asap3.nanoparticle_mc import *
from ase.visualize import view
import matplotlib.pyplot as plt
import numpy as np
if len(sys.argv) < 2:
        print(__doc__, file=sys.stderr)
        sys.exit(1)
#Store path to file
pp = str(sys.argv[1])

#Instantiate d as SurfaceM.. object
d = SurfaceMonteCarloData()
#Read data from file to d
d.read(pp)
surfaces = fcc.surface_names
#Energy
energies = d.arrays['energy'] #The Surf. energies are not relaxed.
val, idx = min((val, idx) for (idx, val) in enumerate(energies))
#Construct atoms
atoms = FaceCenteredCubic('Au', surfaces, d[idx][1], latticeconstant=4.055)
view(atoms) #View atoms

#Now plot the energies
#im =int(np.where(energies==max(energies))[0])

#atoms2 = FaceCenteredCubic('Au', surfaces, d[im][1], latticeconstant=4.055)
#view(atoms2)

#plt.plot(energies)
#plt.ylim(min(energies),0.126)
#plt.show()
