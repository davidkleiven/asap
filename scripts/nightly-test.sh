#! /bin/bash

# Crontab job for testing ASAP and ASE on Niflheim (using Python 3). 
#
#Put this in crontab:
#    17 03 * * * $HOME/development/asap/scripts/nightly-test.sh $HOME/development/nightly-test/niflheim7-python3 intel
#
# Remember to update the path to the script, and the path to the folder
# where the tests are run.  That folder must contain cloned repos of ase 
# and asap.

if [[ $# -ne 2 ]]; then
    echo "Requires exactly two argument (running directory and foss/intel)."
    exit 1
fi

cd "$1"
if [[ ! (-d asap && -d ase) ]]; then
    echo "The running directory must contain ase and asap."
fi

TOOLCHAIN="$2"

# Limit this job to half an hour - exceeding that is a symptom of a defect.
# DISABLED: The Niflheim frontend already has a lower limit.
ulimit -t 1800

# Check for a lock file
LOCKFILE=$PWD/lock.pid
if [ -f $LOCKFILE ]; then
    echo "Lockfile exists, PID = `cat $LOCKFILE`"
    exit 2
fi
echo $$ > $LOCKFILE

# Load dependencies
. /etc/bashrc
if [[ -n "$CPU_ARCH" ]]; then
    export FYS_PLATFORM=Nifl7_${CPU_ARCH}_${TOOLCHAIN}
fi
export EASYBUILD_PREFIX=$HOME/easybuild/$CPU_ARCH
module use $EASYBUILD_PREFIX/modules/all
module purge
module load matplotlib/3.0.0-${TOOLCHAIN}-2018b-Python-3.6.6
module load OpenKIM-API/2.0.0-beta.3-${TOOLCHAIN}-2018b 

ARCH=`uname -m | sed -e 's/ /_/g'`
export PYTHONPATH=`pwd`/ase:`pwd`/asap/Python:`pwd`/asap/${ARCH}:$PYTHONPATH
export PATH=`pwd`/asap/${ARCH}:$PATH

cd ase
git pull -q > /dev/null
if [[ $? -ne 0 ]]; then
    echo "git pull ase FAILED."
    rm $LOCKFILE
    exit 10
fi
find . -name '*.pyc' -delete > /dev/null 2>&1

cd ../asap
git pull -q > /dev/null
if [[ $? -ne 0 ]]; then
    echo "git pull asap FAILED."
    rm $LOCKFILE
    exit 11
fi

git clean -fdxq > /dev/null
if [[ $? -ne 0 ]]; then
    echo "git clean -fdxq FAILED."
    rm $LOCKFILE
    exit 115
fi

make depend-maybe OBJDIR=$ARCH > makedepend.log 2>&1
if [[ $? -ne 0 ]]; then
    echo "make depend-maybe FAILED."
    echo ""
    cat makedepend.log
    rm $LOCKFILE
    exit 12
fi

make all OBJDIR=$ARCH > makeall.log 2>&1
if [[ $? -ne 0 ]]; then
    echo "make all FAILED."
    echo ""
    cat makeall.log
    rm $LOCKFILE
    exit 13
fi

cd Test
python TestAll.py > testserial-devel.log 2>&1
if [[ $? -ne 0 ]]; then
    echo "Serial tests FAILED with developer ase."
    echo ""
    cat testserial-devel.log
    rm $LOCKFILE
    exit 14
fi

mpirun -np 2 asap-python TestAll.py --parallel > testparallel-devel.log 2>&1
if [[ $? -ne 0 ]]; then
    echo "Parallel tests FAILED with developer ase."
    echo ""
    cat testparallel-devel.log
    rm $LOCKFILE
    exit 15
fi

# Now run with system-wide ASE
module load ASE/3.16.2-${TOOLCHAIN}-2018b-Python-3.6.6

python TestAll.py > testserial-inst_ase.log 2>&1
if [[ $? -ne 0 ]]; then
    echo "Serial tests FAILED with installed ase."
    echo ""
    cat testserial-inst_ase.log
    rm $LOCKFILE
    exit 16
fi

mpirun -np 2 asap-python TestAll.py --parallel > testparallel-inst_ase.log 2>&1
if [[ $? -ne 0 ]]; then
    echo "Parallel tests FAILED with install ase."
    echo ""
    cat testparallel-inst_ase.log
    rm $LOCKFILE
    exit 17
fi

rm $LOCKFILE
